package com.felixcool98.managers.data;

import java.util.HashMap;
import java.util.Map;

import com.felixcool98.components.HasData;
import com.felixcool98.components.creation.CreationData;
import com.felixcool98.data.Data;
import com.felixcool98.managers.ComponentManager;

/**
 * manager for all {@link Data} implementations
 * 
 * @author felixcool98
 *
 */
public class DataManager {
	/**all data*/
	private Map<Class<? extends HasData<?>>, Data> objects = new HashMap<Class<? extends HasData<?>>, Data>();
	
	/**the component manager this was created from*/
	private ComponentManager componentManager;
	
	
	//======================================================================
	// Constructors
	//======================================================================
	/**
	 * creates a simple datamanger from a {@link ComponentManager}<br>
	 * data needs to be added manually
	 * 
	 * @param manager the {@link ComponentManager} that is used to create this 
	 */
	public DataManager(ComponentManager manager) {
		this.componentManager = manager;
	}
	public DataManager(ComponentManager manager, CreationData creationData) {
		this.componentManager = manager;
		
		for(HasData<?> hasData : manager.getDataList()) {
			if(!(hasData instanceof HasData.CanCreateData))
				continue;
			
			Data data = ((HasData.CanCreateData<?>) hasData).create(manager, creationData);
			
			add(data);
		}
	}
	
	
	//======================================================================
	// add Data
	//======================================================================
	/**
	 * adds data to the manager
	 * 
	 * @param data the data to add
	 */
	public void add(Data data) {
		objects.put(data.getDataClass(), data);
	}
	
	
	//======================================================================
	// get Data
	//======================================================================
	/**
	 * gets the {@link data} by using the {@link HasData} class it is defined from
	 * 
	 * @param T the {@link Data} implementation of the {@link HasData} class
	 * @param from the {@link HasData} implementation of the data
	 * @return the {@link Data} implementation of the {@link HasData} class
	 */
	@SuppressWarnings("unchecked")
	public <T extends Data> T get(Class<? extends HasData<T>> from) {
		return (T) objects.get(from);
	}
	
	
	//======================================================================
	// normal getters
	//======================================================================
	/**
	 * gets the {@link ComponentManager} this was created from
	 * 
	 * @return the {@link ComponentManager} this was created from
	 */
	public ComponentManager getComponentManager(){
		return componentManager;
	}
	
	
	//======================================================================
	// checks
	//======================================================================
	
	//check for any component
	/**
	 * checks if the manager already has data of this class
	 * 
	 * @param from the class to check for
	 * @return true if it has data of that type, false if it does not
	 */
	public boolean has(Class<? extends HasData<?>> from){
		return objects.containsKey(from);
	}
}
