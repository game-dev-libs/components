package com.felixcool98.managers.filters;

import com.felixcool98.managers.ComponentManager;

/**
 * a filter used in {@link ComponentManager} to disallow certain components from being added
 * 
 * @author felixcool98
 *
 */
public interface ComponentFilter {
	/**
	 * checks if the component is valid
	 * 
	 * @param component the component to test
	 * @return true if it is valid, false if it is invalid
	 */
	public boolean valid(Object component);
}
