package com.felixcool98.managers.filters;

import java.util.LinkedList;
import java.util.List;

/**
 * implementation of {@link ComponentFilter} to allow filtering classes
 * 
 * @author felixcool98
 *
 */
public class ComponentClassFilter implements ComponentFilter {
	/** the classes to filter */
	private List<Class<?>> classes = new LinkedList<>();
	
	/** allows changing between filtering the classes and allowing only these classes */
	boolean disallow = true;
	
	
	/**
	 * creates a filter that either allows only certain classes or filters the classes out
	 * 
	 * @param disallow true if the classes should be filtered out, false if only these classes should be allowed
	 * @param classes the classes to check for
	 */
	public ComponentClassFilter(boolean disallow, Class<?>... classes) {
		this.disallow = disallow;
		
		for(Class<?> classs : classes) {
			this.classes.add(classs);
		}
	}
		
	
	@Override
	public boolean valid(Object component) {
		for(Class<?> classs : classes) {
			if(component.getClass().equals(classs))
				return disallow;
		}
		
		return !disallow;
	}
}
