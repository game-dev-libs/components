package com.felixcool98.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.felixcool98.managers.data.DataManager;
import com.felixcool98.managers.filters.ComponentFilter;
import com.felixcool98.components.ComponentException;
import com.felixcool98.components.HasData;
import com.felixcool98.components.Requires;
import com.felixcool98.components.creation.CreationData;

/**
 * Can store up to one {@link Component} per type<br>
 * <br>
 * Allows filtering by disallowing {@link Component}s from certain classes
 * 
 * @author felixcool98
 */
public class ComponentManager {
	/**The map containing all {@link Component}s*/
	private Map<Class<?>, Object> components = new HashMap<Class<?>, Object>();
	/**the list of filters*/
	private transient List<ComponentFilter> filters = new LinkedList<>();
	
	/**components with data*/
	private List<HasData<?>> dataComponents = new LinkedList<>();
	/**determines how much data needs to be stored per instance*/
	private transient boolean hasData = false;
	
	
	public ComponentManager() {
		
	}
	
	
	public DataManager createDataManager(CreationData data) {
		return new DataManager(this, data);
	}
	
	
	//======================================================================
	// getting Components
	//======================================================================
	/**
	 * getter for {@link Component}s by class
	 * 
	 * @param classs the class of the wanted {@link Component}
	 * @return the {@link Component} of the class or null if none could be found
	 */
	@SuppressWarnings("unchecked")
	public <T> T getComponent(Class<T> classs){
		return (T) components.get(classs);
	}
	
	
	//======================================================================
	// Checks for specific Components
	//======================================================================
	/**
	 * checks if this Manager contains a certain {@link Component}
	 * 
	 * @param classs the class of the component
	 * @return true if the manager contains the component, false if it does not
	 */
	public boolean hasComponent(Class<?> classs){
		return components.containsKey(classs);
	}
	/**
	 * checks if this Manager already contains a {@link Component} of the same type
	 * 
	 * @param component the {@link Component} to check for
	 * @return true if the manager contains a {@link Component} of the same type, false if it does not
	 */
	public boolean hasComponent(Object component){
		return hasComponent(component.getClass());
	}
	
	
	//======================================================================
	// adding components
	//======================================================================
	/**
	 * adds a {@link Component} to the Manager<br>
	 * <br>
	 * disallowed {@link Component}s will be ignored<br>
	 * if the {@link Component} is an instance of {@link HasData} {@link #hasData} will change to true
	 * 
	 * @param component the {@link Component} to add
	 */
	public void addComponent(Object component){
		addComponent(component, component.getClass());
	}
	/**
	 * adds a {@link Component} to the Manager<br>
	 * <br>
	 * disallowed {@link Component}s will be ignored<br>
	 * if the {@link Component} is an instance of {@link HasData} {@link #hasData} will change to true
	 * 
	 * @param component the {@link Component} to add
	 * @param componentClass the class to register the object on
	 */
	public void addComponent(Object component, Class<?> componentClass){
		if(!componentClass.isAssignableFrom(component.getClass()))
			return;
		
		for(ComponentFilter filter : filters) {
			if(!filter.valid(component))
				return;
		}
		
		if(component instanceof Requires) {
			for(Class<?> classs : ((Requires) component).requiredComponents()) {
				if(!hasComponent(classs))
					throw new ComponentException(component.toString()+" requires "+classs.toString());
			}
		}
		
		if(component instanceof HasData) {
			hasData = true;
			dataComponents.add((HasData<?>) component);
		}
		
		components.put(componentClass, component);
	}
	/**
	 * adds a {@link Component} to the Manager<br>
	 * does not add {@link Component}s of types already added to the manager<br>
	 * <br>
	 * disallowed {@link Component}s will be ignored<br>
	 * if the {@link Component} is an instance of {@link HasData} {@link #hasData} will change to true
	 * 
	 * @param component the {@link Component} to add
	 */
	public void addComponentWithCheck(Object component){
		if(hasComponent(component))
			return;
		
		addComponent(component);
	}
	/**
	 * Uses {@link #addComponent(Component)} at every {@link Component} in the list
	 * 
	 * @param components the components to add
	 */
	public void add(List<Object> components) {
		for(Object c : components)
			addComponent(c);
	}
	/**
	 * Uses {@link #addComponent(Component)} at every {@link Component} in the array
	 * 
	 * @param components the components to add
	 */
	public void addComponent(Object...components) {
		for(Object component : components)
			addComponent(component);
	}
	
	
	//======================================================================
	// removing components
	//======================================================================
	/**
	 * removes the {@link Component} from the type from the Manager
	 * 
	 * @param classs the type of the component
	 */
	public void removeComponent(Class<?> classs) {
		Object c = components.remove(classs);
		
		if(c instanceof HasData)
			dataComponents.remove(c);
	}
	
	
	//======================================================================
	// filtering
	//======================================================================
	/**
	 * adds an filter to the manager
	 * 
	 * @param filter the filter to add
	 */
	public void addFilter(ComponentFilter filter) {
		filters.add(filter);
	}
	
	
	//======================================================================
	// data
	//======================================================================
	/**
	 * checks if this manager has any components implementing {@link HasData}
	 * 
	 * @return true if the manager has components which need data
	 */
	public boolean hasData(){
		return hasData;
	}
	/**
	 * gets all components that implement {@link HasData}
	 * 
	 * @return a list of all components needing data
	 */
	public List<HasData<?>> getDataList(){
		return new LinkedList<>(dataComponents);
	}
	
	
	//getting all
	/***
	 * gets all components
	 * 
	 * @return a list with all components
	 */
	public List<Object> getAllComponents(){
		return new ArrayList<>(components.values());
	}
	
	
	//======================================================================
	// Object overrides
	//======================================================================
	@Override
	public String toString() {
		return components.toString();
	}
}
