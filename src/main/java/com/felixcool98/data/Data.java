package com.felixcool98.data;

import com.felixcool98.components.HasData;
import com.felixcool98.managers.data.DataManager;

/**
 * interface used to define per instance data for Components
 * 
 * @author felixcool98
 *
 */
public interface Data {
	/**
	 * used by the {@link DataManager} to sort data
	 * 
	 * @return the class of the {@link Component} this is data of
	 */
	public Class<? extends HasData<?>> getDataClass();
}
