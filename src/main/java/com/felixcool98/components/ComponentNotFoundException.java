package com.felixcool98.components;

public class ComponentNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -438808813277446526L;

	public ComponentNotFoundException(Class<?> classs) {
		super("couldn't find component: " + classs.toString());
	}
}
