package com.felixcool98.components;

public interface Requires {
	public Class<?>[] requiredComponents();
}
