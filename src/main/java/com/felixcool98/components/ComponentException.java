package com.felixcool98.components;

public class ComponentException extends RuntimeException {
	private static final long serialVersionUID = 5409794505478692608L;

	public ComponentException(String text) {
		super(text);
	}
}
