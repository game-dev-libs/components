package com.felixcool98.components;

import com.felixcool98.components.creation.CreationData;
import com.felixcool98.data.Data;
import com.felixcool98.managers.ComponentManager;

/**
 * Allows a {@link Component} to have data per instance<br>
 * Is f.e. used for positions in entities.
 * 
 * @author felixcool98
 *
 * @param <D> the class implementing the data for the instance
 */
public interface HasData<D extends Data> {
	
	
	/**
	 * used for autocreating data
	 * 
	 * @author felixcool98
	 *
	 * @param <D>
	 */
	public static interface CanCreateData <D extends Data> extends HasData<D> {
		public D create(ComponentManager manager, CreationData data);
	}
}
