package com.felixcool98.components.creation;

import java.util.HashMap;

public class CreationData extends HashMap<String, Object> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5224446937484577262L;
	
	
	public float getFloat(String key) {
		return (float) get(key);
	}
}
